// Copyright 2019 The Tango Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package query_test

import (
	"net/http"
	"testing"

	. "gitea.com/lunny/query"
	"github.com/stretchr/testify/assert"
)

func TestQuery(t *testing.T) {
	req, err := http.NewRequest("GET", "http://localhost:8000/?id=1", nil)
	if err != nil {
		t.Error(err)
	}

	id, err := GetQuery(req).Int("id")
	assert.NoError(t, err)
	assert.EqualValues(t, 1, id)

	assert.EqualValues(t, 1, GetQuery(req).MustInt64("id"))
}
