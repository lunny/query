# query

query is a tool to get value from the request url

## Installation

```
go get gitea.com/lunny/query
```

## Usage

```
id := GetQuery(req).MustInt64("query_id)
```